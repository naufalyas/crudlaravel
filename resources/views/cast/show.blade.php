@extends('layout.master')
@section('judul')
    Halaman Detail Cast : {{$cast->nama}}
@endsection

@section('content')

<h3>Caster : {{$cast->nama}}</h3>
<p>Umur : {{$cast->umur}}</p>
<p>Pengisi : {{$cast->bio}}</p>

@endsection